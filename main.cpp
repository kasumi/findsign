#include "SignFinder.h"
#include "utils.h"

#include <algorithm>
#include <iostream>
#include <thread>

void findSign(std::vector<std::string> file_pathse, size_t begin, size_t end, SignFinder::Visualization visualize)
{
	for (size_t i = begin; i < end; ++i)
	{
		auto &path = file_pathse.at(i);

		SignFinder finder;
		if (!finder.init(path))
		{
			std::cerr << "Can't read " << path << std::endl;
			continue;
		}

		auto result = finder.find(visualize);
		std::cout << path << std::endl;
		std::cout << "File size: " << utils::getFilesize(path) << " bytes" << std::endl;
		std::cout << "Circle: " << "\t"
				  << "X: " << result[0] << " Y: " << result[1] << " Radius: " << result[2]
				  << std::endl;
	}

}

void help()
{
	std::cout << "==== Sign finder ====" << "\n"
			  << "Usage: <path_to_image_directory> <extension> <show_result {y | n}>"
			  << "Warning: Multitherading is unavaible in visualization mode"
			  << std::endl;
}

int main(int argc, char** argv)
{
	if (argc != 4)
	{
		help();
		return -1;
	}

	SignFinder::Visualization visualize = std::strcmp(argv[3], "y") ?
				SignFinder::Visualization::OFF : SignFinder::Visualization::ON;

	std::vector<std::string> file_pathes;
	utils::getFilePathes(argv[1], argv[2], file_pathes);

	const size_t thread_count = (visualize == SignFinder::Visualization::ON) ?
				1 : std::max(std::thread::hardware_concurrency(), 1u);

	const size_t part_size = file_pathes.size() / thread_count;
	const size_t remain = file_pathes.size() % thread_count;

	std::vector<std::thread> threads;
	for (size_t i = 0, begin = 0; i < thread_count; ++i)
	{
		size_t current_part_size = part_size + (i < remain ? 1: 0);
		threads.push_back(std::thread(findSign, file_pathes, begin, begin + current_part_size, visualize));
		begin += current_part_size;
	}

	for (auto &thread : threads)
		thread.join();

	return 0;
}

