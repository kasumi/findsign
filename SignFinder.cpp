#include "SignFinder.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

bool SignFinder::init(std::string &path)
{
	this->src = cv::imread(path, CV_LOAD_IMAGE_COLOR);

	if (!src.data)
		return false;

	this->prepair();
	return true;
}

cv::Vec3f SignFinder::find(SignFinder::Visualization visualization) const
{
	std::vector<cv::Vec3f> circles;
	cv::HoughCircles(src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows, 40, 24, 10, 0);

	if (circles.empty())
		return cv::Vec3f(.0f, .0f, .0f);

	auto &result = circles.at(0);
	result /= this->ratio;

	if (visualization == Visualization::ON)
		this->draw(result);

	return result;
}

void SignFinder::prepair()
{
	this->ratio = IMAGE_MAX_SIZE / static_cast<float>(this->src.rows);
	cv::resize(this->src, this->src_gray, cv::Size(this->src.rows * ratio, this->src.cols * this->ratio));

	cv::cvtColor(this->src_gray, this->src_gray, CV_BGR2GRAY);
	cv::equalizeHist(this->src_gray, this->src_gray);
	cv::GaussianBlur(this->src_gray, this->src_gray, cv::Size(5, 5), 2, 2);
}

void SignFinder::draw(cv::Vec3f &circle) const
{
	cv::Mat result = this->src.clone();

	cv::Point center(cvRound(circle[0]), cvRound(circle[1]));
	int radius = cvRound(circle[2]);

	cv::circle(result, center, 3, cv::Scalar(0, 255, 0), -1, 8, 0);
	cv::circle(result, center, radius, cv::Scalar(0, 0, 255), 3, 8, 0);

	cv::imshow("Result", result);
	cv::waitKey(0);
}

