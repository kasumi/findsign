#pragma once

#include <boost/filesystem.hpp>
#include <cstdio>
#include <fstream>
#include <string>

namespace utils {

namespace fs = ::boost::filesystem;

void getFilePathes(const fs::path &root, const std::string &ext, std::vector<std::string> &contents)
{
	if (!fs::exists(root) || !fs::is_directory(root))
		return;

	fs::recursive_directory_iterator it(root);
	fs::recursive_directory_iterator endit;

	for (; it != endit; ++it)
	{
		if (fs::is_regular_file(*it) && it->path().extension() == ext)
			contents.push_back(it->path().string());
	}
}

size_t getFilesize(const std::string &path)
{
	std::ifstream in(path.c_str(), std::ifstream::ate | std::ifstream::binary);
	return in.tellg();
}

} /// namespace utils
