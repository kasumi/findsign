#pragma once

#include <opencv2/core/core.hpp>

#include <cinttypes>

class SignFinder final
{	
public:
	enum class Visualization : uint8_t
	{
		ON,
		OFF
	};

	bool init(std::string &path);
	cv::Vec3f find(Visualization visualization) const;

private:
	const uint8_t IMAGE_MAX_SIZE = 64;

	cv::Mat src;
	cv::Mat src_gray;
	float ratio = .0f;

	void prepair();
	void draw(cv::Vec3f &circle) const;
};
